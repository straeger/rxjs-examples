import * as $ from 'jquery'
import * as Rx from "rxjs/Rx";

const $input =   $('#wsearch');
const $results = $('#wresults');

let searchWikipedia = (term:string) => {
    return $.ajax({
        url: 'https://en.wikipedia.org/w/api.php',
        dataType: 'jsonp',
        data: {
            action: 'opensearch',
            format: 'json',
            search: term
        }
    }).promise();
}

const suggestions = Rx.Observable.fromEvent($input[0], 'keyup')
    .pluck('target', 'value')
    .filter((text:string) => text.length > 2 )
    .debounce(() => Rx.Observable.timer(500))
    .distinctUntilChanged()
    .flatMap(searchWikipedia);

suggestions.subscribe(
  (data:any) => {
    $results
      .empty()
      .append($.map(data[1], (value:any) =>  $('<li>').text(value)));
  },
  (error:any) => {
    $results
      .empty()
      .append($('<li>'))
        .text(`Error: ${error}`);
  });
